[package]
name = "di-api"
description = "The core backend API of Devisa, LLC"
version = "0.1.0"
authors = ["Chris P <chrisp1877@gmail.com>"]
edition = "2018"
default-run = "di-api"

[lib]
name = "api"
path = "api/src/lib.rs"

[[bin]]
name = "di-api"
path = "api/src/bin/server.rs"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies.actix-web]
version = "=4.0.0-beta.7"
features = ["cookies",  "compress", "rustls", "secure-cookies" ]

[dependencies]
api-common = { path = "./api-common/" }
api-lang = { path = "./api-lang/" }
api-rt = { path = "./api-rt/" }
api-redis = { path = "./api-redis/" }
api-proto = { path = "./api-proto/", optional = true }
api-db = { path = "./api-db/" }
actix = "*"
serde = "1.0.125"
async-trait = "*"
serde_json = { version = "1.0.64", features = ["preserve_order"] }
actix-http = { version = "=3.0.0-beta.7" }
# actix-service = { version = "2.0.0" }
# actix-web-actors = {  }
chrono = { version = "0.4.19", features = ["serde"]}
anyhow = "1.0.40"
dotenv = "0.15.0"
uuid = { version = "*", features = ["v4", "serde"] }
actix-multipart = ">=0.4.0-beta.4"
futures = "0.3.14"
pwhash = "1.0.0"
actix-broker = "*"
jsonwebtoken = "7.2.0"
actix-rt = "*"
ring = "0.16.20"
lazy_static = "1.4.0"
futures-util = "0.3.14"
derive_more = "0.99.13"
time = "0.2.26"
redis = { version="0.20.0", features=["aio", "tokio-comp"]}
tokio = "1.5.0"
async-graphql = { version ="2.9.3", features=["chrono", "url", "uuid"] }
async-graphql-actix-web = "2.9.3"
actix-protobuf = { version = "0.6.0", optional = true }
protobuf ={ version = "2.24.1", optional = true }
rustls = { version = "0.19.1", optional = false }
tokio-util = { version = "0.6.7" }
tracing-futures = "0.2.5"
url = { version = "2.2.2", features = ["serde"] }
once_cell = "1.8.0"
tinyvec = { version = "1.2.0", features = ["alloc"] }
env_logger = "0.8.4"
tracing-appender = "0.1.2"
log = "0.4.14"
tracing-log = "0.1.2"
tracing-opentelemetry = { version = "0.13.0", default-features = false }
actix-web-prom = "0.5.1"
sentry = "0.22.0"
sentry-actix = "0.22.0"
tracing = "0.1.26"
# tracing-actix-web = "0.4.0-beta.7"
# actix-redis = { version = "0.10.0-beta.1", features = ["web"] }
# actix-cors = "=0.6.0-beta.1"
# actix-identity = "=0.4.0-beta.1"
# actix-session = { git = "https://github.com/actix/actix-extras/" }


# tracing-actix = "0.3.0"
# acme-client = { version = "0.5", default-features = false }
# actix-web-opentelemetry = "0.10.0"
# bytes = "1.0.1"
# dashmap = "4.0.2"
# pin-project-lite = "0.2.6"
# actix-web-static-files = "3.0.5"
# actix-web-grants = "2.0.1"
# awc = "2.0.3"
# actix-http = "2.2.0"
# juniper = "0.15.4"
# actix-service = { version = "^2.0.0-beta.5" }
# actix-files = "0.6.0-beta.4"
# toml = "0.5.8"
# regex = "1.4.6"
# sentry-actix = "0.22.0"
# once_cell = "1.7.2"
# tinyvec = "1.2.0"
# sentry = "0.22.0"
# actix-web-httpauth = "0.6.0-beta.1"
# actix-cors = "0.6.0-beta.1
# cookie = "0.15"
# actix-web-actors = ">=4.0.0-beta.4"
# actix-identity = "0.4.0-beta.1"
# actix-service = { version = "^2.0.0-beta.5" }
# juniper = { git = "https://github.com/graphql-rust/juniper" }
# juniper_actix = "*"

# [dependencies.libreauth]
# version = "*"
# default-features = false
# features = ["key", "oath", "pass"]


[dependencies.sqlx]
version = "0.5.5"
default-features = false
optional = false
features = [
  "postgres", "runtime-actix-rustls",
  "macros", "chrono",
  "json", "migrate", "uuid"
]

[dependencies.tracing-subscriber]
version = "0.2.18"
default-features = false
optional = false
features = ["env-filter", "registry", "fmt", "smallvec", "tracing-log", "json", "parking_lot"]

[dependencies.opentelemetry]
version = "0.14.0"
efault-features = false
optional = false
features = ["trace", "rt-tokio"]

[features]
default = [ "grpc" ]
grpc = ["api-proto", "protobuf", "actix-protobuf"]

[dev-dependencies]
# criterion = "0.3.4"
# fake = "*"
# tokio = { version = "1.5.0", features = ["macros"] }
# actix-rt = "2.2.0"
# wiremock = "0.5.3"
# actix-http = "2.2.0"
# reqwest = { version = "0.11.3", features = ["blocking","json"] }
# actix-http-test = "2.1.0"
# lazy_static = "1.4.0"
# actix-server = "1.0.4"
# actix-test-server = "0.2.2"

[workspace]

members = [
  "./api/api-gql",
  "./api-rt",
  "./api-common",
  "./api-proto",
  "./api-db",
  "./api-redis",
  "./api-lang",
]

default-members = [
  "./api/api-gql",
  "./api-rt",
  "./api-common",
  "./api-proto",
  "./api-redis",
  "./api-db",
  "./api-lang",
]

# [package]
# name = "dv_api"

# [[bin]]
# name = "api"
# path = "api/src/bin/server.rs"

[profile.dev]
opt-level = 1
overflow-checks = false
debug = true
incremental = true
panic = 'unwind'
lto = false
debug-assertions = true
codegen-units = 256
rpath = false

[profile.release]
opt-level = 3
debug = false
panic = 'unwind'
overflow-checks = false
debug-assertions = false
lto = false
incremental = false
codegen-units = 16
rpath = false

[profile.test]
opt-level = 0
debug = 2
debug-assertions = true
overflow-checks = true
incremental = true
rpath = false
codegen-units = 256
panic = 'unwind'
lto = false

[profile.bench]
opt-level = 3
