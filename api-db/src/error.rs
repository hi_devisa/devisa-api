pub enum DbError {
    ConnectionFailed,
    FailedToAcquireFromPool,
    NotFound,
}
